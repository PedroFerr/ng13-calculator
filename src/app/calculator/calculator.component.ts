import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-calculator',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent {
    
    currentNumber: string = '0';
    firstOperand: number | undefined;
    operator:string | undefined;
    waitForSecondNumber = false;

    getNumber(v: string) {
        console.log(v);
        if (this.waitForSecondNumber) {
            this.currentNumber = v;
            this.waitForSecondNumber = false;
        } else {
            this.currentNumber === '0' ? this.currentNumber = v : this.currentNumber += v;

        }
    }

    getDecimal() {
        if (!this.currentNumber.includes('.')) {
            this.currentNumber += '.';
        }
    }

    getOperation(op: string) {
        console.log(op);

        if(this.firstOperand === undefined) {
            this.firstOperand = Number(this.currentNumber);

        } else if(this.operator) {
            const result: number = this.doCalculation(this.operator, Number(this.currentNumber))
            
            this.currentNumber = String(result);
            this.firstOperand = result;
        }
        this.operator = op;
        this.waitForSecondNumber = true;

        console.log(this.firstOperand);

    }

    clear() {
        this.currentNumber = '0';
        this.firstOperand = 0;
        this.operator = '';
        this.waitForSecondNumber = false;
    }

    private doCalculation(op: string, secondOp: number) {
        if(this.firstOperand !== undefined){
            switch (op) {
                case '+':
                    return this.firstOperand += secondOp;
                case '-':
                    return this.firstOperand -= secondOp;
                case '*':
                    return this.firstOperand *= secondOp;
                case '/':
                    return this.firstOperand /= secondOp;
                case '=':
                    return secondOp;
                default:
                    return secondOp;
            }    
        } else {
            return 0
        }  
    }


}
